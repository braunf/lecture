#!/usr/bin/env python3

import argparse
import re
import collections
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import numpy as np


def parse_log(log_path):
    """
    Returns:
    ttseq: `dict`
        `ttseq[n]` is list of timings of sequential write with N=n
    ttpar: `dict`
        `ttpar[n]` is list of timings of parallel write with N=n
    """
    ttseq = collections.defaultdict(lambda: [])
    ttpar = collections.defaultdict(lambda: [])

    with open(log_path) as f:
        text = f.read()

    mm = re.findall(
        """diffusion .*? .*? (\d*).*
.*by sequential I/O: ([^s]*)s.*
.*by parallel MPI I/O: ([^s]*)s.*""", text, re.MULTILINE)
    for m in mm:
        n = int(m[0])
        tseq = float(m[1])
        tpar = float(m[2])
        ttseq[n].append(tseq)
        ttpar[n].append(tpar)
    return ttseq, ttpar


def plot(log_path, out_path, verbose=True):
    def filesize(n):
        "Returns file size in MiB"
        n = np.array(n)
        return n * (n + 2) * 8 / (1 << 20)

    def throughput(t, n):
        "Returns throughput in MiB/s"
        t = np.array(t)
        n = np.array(n)
        return filesize(n) / t

    ttseq, ttpar = parse_log(log_path)
    nn = sorted(ttseq.keys())
    if verbose:
        header = ["N", "size[MiB]", "size[32KiB]", "seq[MiB/s]", "par[MiB/s]"]
        fmt = "{:<12}" * len(header)
        print(fmt.format(*header))
        for n in nn:
            print(
                fmt.format(n, np.round(filesize(n), 3),
                           np.round(filesize(n) * 1024 / 32, 3),
                           int(throughput(min(ttseq[n]), n)),
                           int(throughput(min(ttpar[n]), n))))

    plt.rcParams.update({'font.size': 8})
    plt.figure(figsize=(3.5, 2.5))
    for tt, lb, c in zip([ttseq, ttpar], ["sequential", "parallel"],
                         ["C0", "C1"]):
        plt.scatter([filesize(n) for n in nn for t in tt[n]],
                    [throughput(t, n) for n in nn for t in tt[n]],
                    c=c,
                    marker='.',
                    alpha=0.5)
        plt.plot(filesize(nn),
                 throughput([min(tt[n]) for n in nn], nn),
                 c=c,
                 label=lb)
    plt.xlabel("file size [MiB]")
    plt.ylabel("throughput [MiB/s]")
    plt.legend()
    plt.xlim(0)
    plt.ylim(0)
    plt.tight_layout()
    plt.savefig(out_path)
    plt.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('log',
                        type=str,
                        help='Path to log file (outputs of `run_test.sh`)')
    parser.add_argument('out',
                        type=str,
                        nargs='?',
                        default="out.pdf",
                        help='Path to output')
    parser.add_argument('--verbose',
                        action="store_true",
                        help='Print grid and file size')
    args = parser.parse_args()
    plot(args.log, args.out, args.verbose)


if __name__ == "__main__":
    main()
