#pragma once

#include <mpi.h>

void mpiDie(const char* filename, int line, int code);

#define MPI_Check(expr)                                                        \
    do {                                                                       \
        const int code = expr;                                                 \
        if (code != MPI_SUCCESS)                                               \
            mpiDie(__FILE__, __LINE__, code);                                  \
    } while (0)
