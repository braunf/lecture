// #include <iostream>
#include <cstdlib>
#include <stdio.h>

int main(int argc, char** argv)
{
    if(argc<2){
      printf("Usage: %s N \n", argv[0]);
      printf("Prints the factorial of N and all the intermediate.\n");
      return 0;
    }
    const int N = atoi(argv[1]);

    // print the header
    printf("Calculating the factorial: %d!\n",N);
    // std::cout << "n" << std::setw(15) << "n!" << std::endl;

    int product = 1;
    for(int n = 0; n <= N; ++n)
    {
        if (n != 0)
        {
            // calculate the next next factorial
            product *= n;
        }

        // output the factorial
        printf("%3d  %15d\n",n,product);
    }

    // tell the caller program that no error has occurred
    return 0;
}
