#include <algorithm>
#include <array>
#include <cassert>
#include <cmath>
#include <fstream>
#include <functional>
#include <iostream>
#include <mpi.h>
#include <sstream>
#include <unordered_map>
#include <vector>

using Point = std::array<double, 3>;
const size_t kNumPoints = 4;
using Polygon = std::array<Point, kNumPoints>;
using PolygonIdx = std::array<int, kNumPoints>;

std::vector<Polygon> get_polygons(int rank, int commsize, int N, int t)
{
    std::vector<Point> vv;
    const int ppr = 12;
    const double rad = 0.2;
    // helix
    for (int i = rank * N / commsize; i < (rank + 1) * N / commsize; ++i) {
        int j = i - (ppr + 1) * rank;
        double zc = 4 * double(j) / N;
        double phi = 2 * M_PI * (zc + t * 0.1);
        double xc = std::cos(phi);
        double yc = std::sin(phi);

        double om = 2 * M_PI * (j % ppr) / ppr;
        double x = xc + std::cos(om) * xc * rad;
        double y = yc + std::cos(om) * yc * rad;
        double z = zc + std::sin(om) * rad;

        vv.push_back({x, y, z});
    }
    std::vector<Polygon> polygons;
    for (size_t i = 0; i + ppr + 1 < vv.size(); ++i) {
        polygons.push_back({vv[i], vv[i + 1], vv[i + ppr + 1], vv[i + ppr]});
    }
    return polygons;
}

// Converts polygons to points and indices.
void convert_to_idx(const std::vector<Polygon>& pp, std::vector<Point>& points,
                    std::vector<PolygonIdx>& indices)
{
    points.resize(0);
    indices.resize(0);
    for (auto& p : pp) {
        indices.emplace_back();
        for (size_t i = 0; i < p.size(); ++i) {
            indices.back()[i] = points.size();
            points.push_back(p[i]);
        }
    }
}

void merge_duplicates(std::vector<Point>& points,
                      std::vector<PolygonIdx>& indices)
{
    struct Hash {
        size_t operator()(const Point& p) const noexcept
        {
            const size_t h0 = std::hash<double>{}(p[0]);
            const size_t h1 = std::hash<double>{}(p[1]);
            const size_t h2 = std::hash<double>{}(p[2]);
            return h0 ^ (h1 << 1) ^ (h2 << 2);
        }
    };

    std::unordered_map<Point, size_t, Hash> point_to_new; // point to new index
    std::vector<Point> new_points;
    std::vector<size_t> old_to_new;

    for (size_t i = 0; i < points.size(); ++i) {
        const Point p = points[i];
        auto it = point_to_new.find(p);
        if (it != point_to_new.end()) { // existing point
            old_to_new.push_back(it->second);
        } else { // new point
            point_to_new[p] = new_points.size();
            old_to_new.push_back(new_points.size());
            new_points.push_back(p);
        }
    }

    std::vector<PolygonIdx> new_indices;
    for (auto& ii : indices) {
        new_indices.emplace_back();
        for (size_t q = 0; q < kNumPoints; ++q) {
            new_indices.back()[q] = old_to_new[ii[q]];
        }
    }
    points = new_points;
    indices = new_indices;
}

// Writes polygons to legacy vtk file by gathering data on one rank.
void write_gather(const std::string& path, const std::vector<Polygon>& pp,
                  const std::string& comment, int rank, MPI_Comm comm)
{
    int commsize;
    MPI_Comm_size(comm, &commsize);

    int cnt = pp.size() * sizeof(Polygon);

    if (rank == 0) {
        std::vector<int> cnts(commsize);
        MPI_Gather(&cnt, 1, MPI_INT, cnts.data(), 1, MPI_INT, 0, comm);

        std::vector<int> displs(commsize + 1);
        displs[0] = 0;
        for (int i = 0; i < commsize; ++i) {
            displs[i + 1] = displs[i] + cnts[i];
        }

        assert(displs[commsize] % sizeof(Polygon) == 0);
        std::vector<Polygon> pp_all(displs[commsize] / sizeof(Polygon));

        // XXX: Binary serialization, used here for simplicity,
        // is invalid on heterogeneous systems.
        // MPI_Datatype describing Point should be used instead.

        MPI_Gatherv(pp.data(), cnt, MPI_CHAR, pp_all.data(), cnts.data(),
                    displs.data(), MPI_CHAR, 0, comm);

        std::vector<Point> points;
        std::vector<PolygonIdx> indices;
        convert_to_idx(pp_all, points, indices);
        merge_duplicates(points, indices);

        std::ofstream fout(path);
        fout << "# vtk DataFile Version 2.0\n"
             << comment + "\n"
             << "ASCII\n"
             << "DATASET POLYDATA\n";

        fout << "POINTS " << points.size() << " float\n";
        for (auto& p : points) {
            fout << p[0] << ' ' << p[1] << ' ' << p[2] << '\n';
        }

        fout << "POLYGONS  " << indices.size() << ' '
             << indices.size() * (1 + kNumPoints) << '\n';
        for (auto& ii : indices) {
            fout << ii.size();
            for (auto i : ii) {
                fout << ' ' << i;
            }
            fout << '\n';
        }
    } else {
        MPI_Gather(&cnt, 1, MPI_INT, nullptr, 0, MPI_INT, 0, comm);
        MPI_Gatherv(pp.data(), cnt, MPI_CHAR, nullptr, nullptr, nullptr,
                    MPI_CHAR, 0, comm);
    }
}

// Writes polygons to legacy vtk file using MPI IO.
void write_mpi(const std::string& path, const std::vector<Polygon>& pp,
               const std::string& comment, int rank, MPI_Comm comm)
{
    std::vector<Point> points;
    std::vector<PolygonIdx> indices;
    convert_to_idx(pp, points, indices);

    MPI_File fout;
    MPI_File_open(comm, path.data(), MPI_MODE_CREATE | MPI_MODE_WRONLY,
                  MPI_INFO_NULL, &fout);

    const int npoints = points.size();
    int sum_npoints = 0;
    MPI_Reduce(&npoints, &sum_npoints, 1, MPI_INT, MPI_SUM, 0, comm);

    if (rank == 0) {
        std::stringstream s;
        s << "# vtk DataFile Version 2.0\n"
          << comment + "\n"
          << "ASCII\n"
          << "DATASET POLYDATA\n"
          << "POINTS " << sum_npoints << " float\n";
        const std::string str = s.str();
        MPI_File_write_shared(fout, str.data(), str.size(), MPI_CHAR,
                              MPI_STATUS_IGNORE);
    }

    {
        std::stringstream s;
        for (auto& p : points) {
            s << p[0] << ' ' << p[1] << ' ' << p[2] << '\n';
        }
        const std::string str = s.str();
        MPI_File_write_ordered(fout, str.data(), str.size(), MPI_CHAR,
                               MPI_STATUSES_IGNORE);
    }

    size_t npoly = indices.size();
    size_t nindices = 0;
    for (auto& p : indices) {
        nindices += p.size();
    }
    int sum_npoly = 0;
    int sum_nindices = 0;
    MPI_Reduce(&npoly, &sum_npoly, 1, MPI_INT, MPI_SUM, 0, comm);
    MPI_Reduce(&nindices, &sum_nindices, 1, MPI_INT, MPI_SUM, 0, comm);

    if (rank == 0) {
        std::stringstream s;
        s << "POLYGONS  " << sum_npoly << ' ' << sum_npoly + sum_nindices
          << '\n';
        const std::string str = s.str();
        MPI_File_write_shared(fout, str.data(), str.size(), MPI_CHAR,
                              MPI_STATUS_IGNORE);
    }

    int offset = 0; // offset for index
    MPI_Exscan(&npoints, &offset, 1, MPI_INT, MPI_SUM, comm);

    {
        std::stringstream s;
        for (auto& p : indices) {
            s << p.size();
            for (auto& k : p) {
                s << ' ' << k + offset;
            }
            s << '\n';
        }
        std::string str = s.str();
        MPI_File_write_ordered(fout, str.data(), str.size(), MPI_CHAR,
                               MPI_STATUSES_IGNORE);
    }
    MPI_File_close(&fout);
}

int main(int argc, char* argv[])
{
    MPI_Init(&argc, &argv);
    MPI_Comm comm = MPI_COMM_WORLD;

    int size, rank;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);

    const int N = 1000;
    const int tmax = 9;
    for (int t = 0; t <= tmax; ++t) {
        if (rank == 0) {
            std::cout << "t=" << t << std::endl;
        }
        const auto pp = get_polygons(rank, size, N, t);
        const auto st = std::to_string(t);
        write_gather("poly_" + st + ".vtk", pp, "comment", rank, comm);
        write_mpi("polympi_" + st + ".vtk", pp, "comment", rank, comm);
    }

    MPI_Finalize();
}
